<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Image extends Model
{
    //
    public function storeImage($img){
        try {
            //code...
            /* $year = $product->created_at->format('Y');
            $month = $product->created_at->format('M');
            $company = Company::find($product->company_id);
            $companyname = str_replace(' ','',$company->display_name);*/
            $fileInfo = pathinfo($img->getClientOriginalName());
            $uri = '/images' ; //. '/'. $companyname .  '/'. $year . '/'. $month;
            $this->path =  public_path() .'/uploads' . $uri ;
            $this->url = '/uploads' . $uri. '/';
            $date = date('dmYhis', time());
            $this->file =  $date . '.' . $fileInfo['extension'];
    
            $img->move($this->path, $this->file);
        } catch (\Exception $exception) {
            //throw $th;
            Log::error($exception);
        }
    }

}
