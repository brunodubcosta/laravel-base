<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Notification;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function client()
    {
        return $this->hasOne(Client::class);
    }

    public function contact()
    {
        return $this->hasOne(Contact::class);
    }

    public function hasPermissionsOrRole($permissions_array)
    {
        foreach($permissions_array as $permission){
            $roles = Role::whereIn('id',Permission_role::whereIn('permission_id',
            Permission::where('name',$permission)->pluck('id')->toarray()
            )->pluck('role_id')->toarray())->get();

            if(!$this->hasAnyRole($roles ?? []) && !$this->hasPermission($permission) ){
                return false; 
            }
        }    
        return true;
    }

    public function getBarcodesBadge(){
        $format = '<span class="badge badge-%s">%d</span>';
        $ds = DesignProduct::whereNotNull('bar_code_id')->get()->pluck('bar_code_id')->toArray();
        $codes = BarCode::whereNotIn('id',$ds)->get();
        $count = count($codes);
        $type = $count > 100 ? 'success' : 
        $count > 50 ? 'warning' :
        'danger';
        return sprintf($format, $type, $count);
    }

    public function getOrdersBadge(){
        $format = '<span class="badge badge-%s">%d</span>';
        $orders = Order::where('state_id',1)->get();
        $count = count($orders);
        $type = $count > 10 ? 'danger' : $count > 0 ? 'warning' :'success';
        return sprintf($format, $type, $count);
    }

    public function getContactsBadge(){
        $format = '<span class="badge badge-%s">%d</span>';
        $orders = ContactForm::where('seen',false)->get();
        $count = count($orders);
        $type = $count > 10 ? 'danger' :
        'warning';
        return $count > 0 ? sprintf($format, $type, $count) : "";
    }

    public function hasAnyPermissionsOrRole($permissions_array)
    {
        foreach($permissions_array as $permission){

            $roles = Role::whereIn('id',Permission_role::whereIn('permission_id',
            Permission::where('name',$permission)->pluck('id')->toarray()
            )->pluck('role_id')->toarray())->get();

            if($this->hasAnyRole($roles) || $this->hasPermission($permission) ){
                return true; 
            }
        }    
        return false;
    }

    public function hasAnyRole($roles_array){
        foreach ($roles_array as $role) {
            if($this->hasRole($role->name))
                return true;
        }
        return false;
    }
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        //$this->notify(new \App\Notifications\MailResetPasswordNotification($token));
        Notification::send($this, new \App\Notifications\MailResetPasswordNotification($token)); 
    }
}
