<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use Kris\LaravelFormBuilder\FormBuilderTrait;

use App\Forms\ChangePasswordForm;
use App\Forms\UserForm;

use App\Rules\CanCreateUser;

use App\User;

class UsersController extends Controller
{
    use FormBuilderTrait;
    
    public function __construct()
    {
        $this->middleware(['auth', 'isActive']);
    }
	
    public function index()
    {
        $users = User::with('roles')->get();
    	return view('backoffice.users.index', compact('users'));
    }

    public function create()
    {
        $form = $this->form(UserForm::class, [
            'method' => 'POST',
            'url' => route('dashboard.users.store')
        ]);
    	return view('backoffice.users.create', compact('form'));
    }

    public function store(Request $request)
    {
    	$result = true;
        $form = $this->form(UserForm::class);
        
        $form->validate([
            'email' => new CanCreateUser,
        ]);

        $form->redirectIfNotValid();

        try {
            DB::beginTransaction();
            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make(Str::random(8));
            $user->is_active = $request->input('is_active') ?? true;
            $user->save();

            $roles = $request->input('role');
            if(is_array($roles)){
                foreach ($roles as $role) {
                    $user->attachRole($role);
                }
            }

            $permissions = $request->input('permission');
            if(is_array($permissions)){
                foreach ($permissions as $permission) {
                    $user->attachRole($permission);
                }
            }
            
            $token = app(\Illuminate\Auth\Passwords\PasswordBroker::class)->createToken($user);
            $user->sendPasswordResetNotification($token);

            DB::commit();
            $result = true;
        } catch (\Exception $e) {
            DB::rollBack();
            
            $result = false;
        }
        
        if($result):
            flash(__('Utilizador criado com sucesso!'))->success();
        else:
            flash(__('Ocurreu um erro ao criar o Utilizador!' . $e))->error();
        endif;

        return redirect()->route('dashboard.users.index');
    }

    public function edit(User $user)
    {
    	$form = $this->form(UserForm::class, [
            'method' => 'PATCH',
            'url' => route('dashboard.users.update', $user),
            'model' => $user
        ]);
        return view('backoffice.users.edit', compact('form'));
    }

    public function update(Request $request, User $user)
    {
    	$result = true;
        $form = $this->form(UserForm::class);
        $form->validate();

        if(User::where('email',$request->input('email'))->where('id','<>',$user->id)->whereNull('deleted_at')->first() != null){
            flash(__('Já existe um utilizador activo com esse email!'))->error();
            return redirect()->back();
        }
            

        $form->redirectIfNotValid();

        try {
            DB::beginTransaction();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->is_active = $request->input('is_active') ?? true;
            $user->update();

            $user->roles()->sync([]);
            $roles = $request->input('role');
            if(is_array($roles)){
                foreach ($roles as $role) {
                    $user->attachRole($role);
                }
            }

            $user->permissions()->sync([]);
            $permissions = $request->input('permission');
            if(is_array($permissions)){
                foreach ($permissions as $permission) {
                    $user->attachPermission($permission);
                }
            }

            DB::commit();
            $result = true;
        } catch (\Exception $e) {
            DB::rollBack();
            $result = false;
        }
        
        if($result):
            flash(__('Utilizador actualizado com sucesso!'))->success();
        else:
            flash(__('Ocurreu um erro ao actualizar o Utilizador!' . $e))->error();
        endif;

        return redirect()->route('dashboard.users.index');
    }

    public function delete(User $user)
    {
    	$result = true;

        try {
            DB::beginTransaction();

            $user->delete();

            DB::commit();
            $result = true;
        } catch (\Exception $e) {
            DB::rollBack();
            $result = false;
        }

        if($result):
            flash(__('Utilizador eliminado com sucesso!'))->success();
        else:
            flash(__('Ocurreu um erro ao eliminar o Utilizador!'))->error();
        endif;
        return redirect()->back();
    }

    public function changePasswordForm()
    {
        $form = $this->form(ChangePasswordForm::class, [
            'method' => 'POST',
            'url' => route('dashboard.profile.change-password.save')
        ]);

        return view('backoffice.profile.change_password', compact('form'));
    }

    public function changePassword(Request $request)
    {
        $result = true;
        $form = $this->form(ChangePasswordForm::class);

        if(!$form->isValid()):
            return response()->json($form->getErrors(), 422);
        endif;

        try {
            DB::beginTransaction();
            
            $user = User::find(Auth::user()->id);
            $user->password = Hash::make($request->input('new_password'));
            $user->update();

            DB::commit();
            $result = true;
        } catch (\Exception $e) {
            DB::rollBack();
            $result = false;
        }
        
        if($result):
            flash(__('Password Alterada com successo!'))->success();
        else:
            flash(__('Ocurreu ao alterar a sua password!'))->error();
        endif;

        return response()->json($result);
    }
}
