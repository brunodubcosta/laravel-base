<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Kris\LaravelFormBuilder\FormBuilderTrait;

use App\Forms\RoleForm;

use App\Role;

class RolesController extends Controller
{
    use FormBuilderTrait;

    public function __construct()
    {
        $this->middleware(['auth', 'isActive']);
    }
	
    public function index()
    {
        $roles = Role::all();
    	return view('backoffice.roles.index', compact('roles'));
    }   

    public function create()
    {
        $form = $this->form(RoleForm::class, [
            'method' => 'POST',
            'url' => route('dashboard.roles.store')
        ]);

    	return view('backoffice.roles.create', compact('form'));
    }

    public function store(Request $request)
    {
        $result = true;
    	$form = $this->form(RoleForm::class);
        
        if(!$form->isValid()):
            return response()->json($form->getErrors(), 422);
        endif;

        try {
            DB::beginTransaction();
            $role = new Role;
            $role->name = $request->input('name');
            $role->display_name = $request->input('display_name');
            if ($request->has('description')):
                $role->description = $request->input('description');
            endif;
            $role->save();

            if($request->input('permissions') != null && is_array($request->input('permissions')))
                $role->attachPermissions($request->input('permissions'));

            DB::commit();
            $result = true;
        } catch (\Exception $e) {
            DB::rollBack();
            $result = false;
            $log = new Log();
            $log->user_id = Auth::user()->id;
            $log->model = "RolesController@store";
            $log->model_id = 0;
            $log->action = $e;
            $log->save();
        }
        
        if($result):
            flash(__('Cargo criado com sucesso!'))->success();
        else:
            flash(__('Ocurreu um erro ao criar o cargo!'))->error();
        endif;

        return redirect(route('dashboard.roles.index'));
    }

    public function edit(Role $role)
    {
         $form = $this->form(RoleForm::class, [
            'method' => 'PATCH',
            'url' => route('dashboard.roles.update', $role),
            'model' => $role
        ]);
        
        /*if($role->name == 'administrador'){
            flash(__('Cargo administrador não pode ser editado.'))->error();
            return redirect(route('dashboard.roles.index'));
        }*/
    	return view('backoffice.roles.edit', compact('form'));
    }

    public function update(Request $request, Role $role)
    {
    	$result = true;
        $form = $this->form(RoleForm::class);
        
        if(!$form->isValid()):
            return response()->json($form->getErrors(), 422);
        endif;

        try {
            DB::beginTransaction();
            $role->name = $request->input('name');
            $role->display_name = $request->input('display_name');
            if ($request->has('description')):
                $role->description = $request->input('description');
            endif;
            $role->update();

            if($request->input('permissions') != null && is_array($request->input('permissions')))
                $role->permissions()->sync($request->input('permissions'));

            DB::commit();
            $result = true;
        } catch (\Exception $e) {
            DB::rollBack();
            $result = false;
            $log = new Log();
            $log->user_id = Auth::user()->id;
            $log->model = "RolesController@update";
            $log->model_id = 0;
            $log->action = $e;
            $log->save();
        }
        
        if($result):
            flash(__('Cargo actualizado com sucesso!'))->success();
        else:
            flash(__('Ocurreu um erro ao actualizar o cargo!'))->error();
        endif;

        return redirect(route('dashboard.roles.index'));
    }

    public function delete(Role $role)
    {
        $result = true;
        if($role->deletable == true){
            try {
                DB::beginTransaction();
    
                $role->delete();
    
                DB::commit();
                $result = true;
            } catch (\Exception $e) {
                DB::rollBack();
                $result = false;
            }

            if($result):
                flash(__('Cargo eliminado com sucesso!'))->success();
            else:
                flash(__('Ocurreu um erro ao eliminar o cargo!'))->error();
            endif;
        }else{
            flash(__('Cargo '.$role->display_name .' não pode ser apagado!'))->error();
        }
        
        return redirect(route('dashboard.roles.index'));
    }
}
