<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Kris\LaravelFormBuilder\FormBuilderTrait;

use App\Forms\PermissionForm;

use App\Permission;

class PermissionsController extends Controller
{
    use FormBuilderTrait;

    public function __construct()
    {
        $this->middleware(['auth', 'isActive']);
    }
	
    public function index()
    {
        $permissions = Permission::all();
        return view('backoffice.permissions.index', compact('permissions'));
    }   

    public function create()
    {
        $form = $this->form(PermissionForm::class, [
            'method' => 'POST',
            'url' => route('dashboard.permissions.store')
        ]);

        return view('backoffice.permissions.create', compact('form'));
    }

    public function store(Request $request)
    {
        $result = true;
        $form = $this->form(PermissionForm::class);
        
        if(!$form->isValid()):
            return response()->json($form->getErrors(), 422);
        endif;

        try {
            DB::beginTransaction();
            $permission = new Permission;
            $permission->name = $request->input('name');
            $permission->display_name = $request->input('display_name');
            if ($request->has('description')):
                $permission->description = $request->input('description');
            endif;
            $permission->save();

            DB::commit();
            $result = true;
        } catch (\Exception $e) {
            DB::rollBack();
            $result = false;
        }
        
        if($result):
            flash(__('Permissão criada com sucesso!'))->success();
        else:
            flash(__('Ocurreu um erro ao criar a permissão!'))->error();
        endif;

        return response()->json($result);
    }

    public function edit(Permission $permission)
    {
         $form = $this->form(PermissionForm::class, [
            'method' => 'PATCH',
            'url' => route('dashboard.permissions.update', $permission),
            'model' => $permission
        ]);
        return view('backoffice.permissions.edit', compact('form'));
    }

    public function update(Request $request, Permission $permission)
    {
        $result = true;
        $form = $this->form(PermissionForm::class);
        
        if(!$form->isValid()):
            return response()->json($form->getErrors(), 422);
        endif;

        try {
            DB::beginTransaction();
            $permission->name = $request->input('name');
            $permission->display_name = $request->input('display_name');
            if ($request->has('description')):
                $permission->description = $request->input('description');
            endif;
            $permission->update();

            DB::commit();
            $result = true;
        } catch (\Exception $e) {
            DB::rollBack();
            $result = false;
        }
        
        if($result):
            flash(__('Permissão actualizada com sucesso!'))->success();
        else:
            flash(__('Ocurreu um erro ao actualizar a Permissão!'))->error();
        endif;

        return response()->json($result);
    }

    public function delete(Permission $permission)
    {
        $result = true;

        try {
            DB::beginTransaction();

            $permission->delete();

            DB::commit();
            $result = true;
        } catch (\Exception $e) {
            DB::rollBack();
            $result = false;
        }

        if($result):
            flash(__('Permissão eliminada com sucesso!'))->success();
        else:
            flash(__('Ocurreu um erro ao eliminar a permissão!'))->error();
        endif;
        return redirect()->back();
    }
}
