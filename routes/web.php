<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [
	'uses' => 'HomeController@index',
	'as' => 'index'
]);


//region Users
Route::get('/dashboard/users', [
	'uses' => 'UsersController@index',
	'middleware' => ['hasPermission:ver_utilizadores'],
	'as' => 'dashboard.users.index'
]);

Route::get('/dashboard/users/create', [
	'uses' => 'UsersController@create',
	'middleware' => ['hasPermission:criar_utilizadores'],
	'as' => 'dashboard.users.create'
]);

Route::post('/dashboard/users/create/store', [
	'uses' => 'UsersController@store',
	'middleware' => ['hasPermission:criar_utilizadores'],
	'as' => 'dashboard.users.store'
]);

Route::get('/dashboard/users/show/{user}', [
	'uses' => 'UsersController@show',
	'middleware' => ['hasPermission:ver_utilizadores'],
	'as' => 'dashboard.users.show'
]);

Route::get('/dashboard/users/edit/{user}', [
	'uses' => 'UsersController@edit',
	'middleware' => ['hasPermission:editar_utilizadores'],
	'as' => 'dashboard.users.edit'
]);

Route::patch('/dashboard/users/edit/{user}/update', [
	'uses' => 'UsersController@update',
	'middleware' => ['hasPermission:editar_utilizadores'],
	'as' => 'dashboard.users.update'
]);

Route::get('/dashboard/users/delete/{user}', [
	'uses' => 'UsersController@delete',
	'middleware' => ['hasPermission:apagar_utilizadores'],
	'as' => 'dashboard.users.delete'
]);
//endregion


//region Roles
Route::get('/dashboard/roles', [
	'uses' => 'RolesController@index',
	'middleware' => ['hasPermission:ver_cargos'],
	'as' => 'dashboard.roles.index'
]);

Route::get('/dashboard/roles/create', [
	'uses' => 'RolesController@create',
	'middleware' => ['hasPermission:criar_cargos'],
	'as' => 'dashboard.roles.create'
]);

Route::post('/dashboard/roles/create/store', [
	'uses' => 'RolesController@store',
	'middleware' => ['hasPermission:criar_cargos'],
	'as' => 'dashboard.roles.store'
]);

Route::get('/dashboard/roles/edit/{role}', [
	'uses' => 'RolesController@edit',
	'middleware' => ['hasPermission:editar_cargos'],
	'as' => 'dashboard.roles.edit'
]);

Route::patch('/dashboard/roles/edit/{role}/update', [
	'uses' => 'RolesController@update',
	'middleware' => ['hasPermission:editar_cargos'],
	'as' => 'dashboard.roles.update'
]);

Route::get('/dashboard/roles/delete/{role}', [
	'uses' => 'RolesController@delete',
	'middleware' => ['hasPermission:apagar_cargos'],
	'as' => 'dashboard.roles.delete'
]);
//endregion

//region Profile
Route::get('/profile', [
	'uses' => 'ProfileController@index',
	//'middleware' => [],
	'as' => 'profile.index'
]);

Route::get('/profile/change-password', [
	'uses' => 'UsersController@changePasswordForm',
	//'middleware' => [],
	'as' => 'profile.change-password'
]);

Route::post('/profile/change-password/save', [
	'uses' => 'UsersController@changePassword',
	//'middleware' => [],
	'as' => 'profile.change-password.save'
]);
//endregion

//region Roles
Route::get('/roles', [
	'uses' => 'RolesController@index',
	'middleware' => ['hasPermission:ver_cargos'],
	'as' => 'roles.index'
]);

Route::get('/roles/create', [
	'uses' => 'RolesController@create',
	'middleware' => ['hasPermission:criar_cargos'],
	'as' => 'roles.create'
]);

Route::post('/roles/create/store', [
	'uses' => 'RolesController@store',
	'middleware' => ['hasPermission:criar_cargos'],
	'as' => 'roles.store'
]);

Route::get('/roles/edit/{role}', [
	'uses' => 'RolesController@edit',
	'middleware' => ['hasPermission:editar_cargos'],
	'as' => 'roles.edit'
]);

Route::patch('/roles/edit/{role}/update', [
	'uses' => 'RolesController@update',
	'middleware' => ['hasPermission:editar_cargos'],
	'as' => 'roles.update'
]);

Route::get('/roles/delete/{role}', [
	'uses' => 'RolesController@delete',
	'middleware' => ['hasPermission:apagar_cargos'],
	'as' => 'roles.delete'
]);
//endregion



//region Permissions
Route::get('/permissions', [
	'uses' => 'PermissionsController@index',
	//'middleware' => [],
	'as' => 'permissions.index'
]);

Route::get('/permissions/create', [
	'uses' => 'PermissionsController@create',
	//'middleware' => [],
	'as' => 'permissions.create'
]);

Route::post('/permissions/create/store', [
	'uses' => 'PermissionsController@store',
	//'middleware' => [],
	'as' => 'permissions.store'
]);

Route::get('/permissions/show/{permission}', [
	'uses' => 'PermissionsController@show',
	//'middleware' => [],
	'as' => 'permissions.show'
]);

Route::get('/permissions/edit/{permission}', [
	'uses' => 'PermissionsController@edit',
	//'middleware' => [],
	'as' => 'permissions.edit'
]);

Route::patch('/permissions/edit/{permission}/update', [
	'uses' => 'PermissionsController@update',
	//'middleware' => [],
	'as' => 'permissions.update'
]);

Route::get('/permissions/delete/{permission}', [
	'uses' => 'PermissionsController@delete',
	//'middleware' => [],
	'as' => 'permissions.delete'
]);
//endregion


Route::get('/language/{locale}', [
    'uses' => 'HomeController@changeLanguage',
	'as' => 'language.change'
]);
Auth::routes(['register' => false]);