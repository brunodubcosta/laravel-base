<?php
return [
    "failed" => "These credentials do not match our records",
    "throttle" => "To many login attempts. Please try again in :seconds seconds."
];
