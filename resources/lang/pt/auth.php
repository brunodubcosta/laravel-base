<?php
return [
    "failed" => "Estas credenciais não coincidem com os nossos registos",
    "throttle" => "Muitas tentativas de login. Por favor tente novamente em :seconds seconds."
];
