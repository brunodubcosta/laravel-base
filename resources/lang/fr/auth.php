<?php
return [
    "failed" => "Ces informations d'identification ne correspondent pas à nos enregistrements",
    "throttle" => "Nombreuses tentatives de connexion. Veuillez réessayer en :seconds secondes."
];
