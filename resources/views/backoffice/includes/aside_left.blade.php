<nav id="sidebar">
    <div class="sidebar-header">
        <div id="sidebar-brand-mage" class="text-center">
            <a class="left-logo" href="#">
                <img id="left-logo-img" src="{{ asset('/images/logo_1.png') }}" />
            </a>
        </div>
        <h3>{{ str_replace('.', ' ', config('app.name')) }}</h3>
    </div>
    <ul class="list-unstyled components">
    {{--<li>
            <a href="{{ route('index') }}" class="collapsed" data-parent="#sidebar">
                <i class="fas fa-tachometer-alt"></i>
                <span class="hidden-sm-down"> {{ __('Dashboard') }} </span>
            </a>
        </li> --}}
        

        @if(Auth::user()->hasAnyPermissionsOrRole(['ver_utilizadores','ver_cargos']))
        <li>
            <a href="#pageSubmenuUsers" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-users"></i>
                <span class="hidden-sm-down"> {{ __('Utilizadores') }}</span>
            </a>
            <ul class="collapse list-unstyled" id="pageSubmenuUsers">
                @if(Auth::user()->hasPermissionsOrRole(['ver_utilizadores']))<li><a href="{{ route('dashboard.users.index') }}">{{ __('Utilizadores') }}</a></li>@endif
                @if(Auth::user()->hasPermissionsOrRole(['ver_cargos']))<li><a href="{{ route('dashboard.roles.index') }}">{{ __('Cargos') }}</a></li>@endif
                <li><a href="route('dashboard.permissions.index')">{{ __('Permissões') }}</a></li>
            </ul>
        </li>
        @endif

        <hr>
        <li>
            <a href="#pageSubmenuProfile" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <i class="fas fa-user"></i>
                <span class="hidden-sm-down">{{ Auth::user()->name }}</span>
            </a>
            <ul class="collapse list-unstyled" id="pageSubmenuProfile">

                <a class="collapsed" data-parent="#sidebar" href="{{ route('profile.index') }}">{{ __('Minha Conta') }}</a>
                <a href="#" class="collapsed" id="change-password-trigger" data-parent="#sidebar">
                    <span class="hidden-sm-down">{{ __('Alterar Password') }}</span>
                </a>
                <a href="#" class="collapsed" data-parent="#sidebar">
                    <span class="hidden-sm-down">{{ __('Ultimo Acesso:') }}</span><br>
                    <span class="hidden-sm-down"> {{ Auth::user()->last_login }} </span>
                </a>
            </ul>
        </li>
        <li>
            <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="collapsed" data-parent="#sidebar">
                <i class="fas fa-sign-out-alt"></i><span class="hidden-sm-down"> {{ __('Logout') }}</span>
            </a>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</nav>