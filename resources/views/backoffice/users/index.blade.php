@extends('layouts.backoffice_master')

@section('head-meta')
	<title>{{ str_replace('.', ' ', config('app.name')) }} - {{ __('Utilizadores') }}</title>
@endsection

@section('head-scripts')
	{{-- expr --}}
@endsection

@section('content')
<div class="row">
		<div class="col">
			
		</div>
	</div>
	<div class="row">
		<div class="col">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col">
							<h5 class="card-title">{{ __('Utilizadores') }}</h5>
						</div>
					</div>
					<users-table :users="{{ $users }}"></users-table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('foot-scripts')
@endsection