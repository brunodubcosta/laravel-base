@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verificação do seu Email') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Um novo link de verificação foi enviado para o seu email.') }}
                        </div>
                    @endif
                    
                    {{ __('Antes de proceguir, verifique o seu email.') }}
                    {{ __('Se não recebeu o email') }}, <a href="{{ route('verification.resend') }}">{{ __('clique aqui para enviar-mos outro') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
