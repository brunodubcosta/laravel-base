@extends('layouts.backoffice_master_nm')

@section('content')
    <section id="login">
        <div class="wrapper fadeInDown" style="height: 100vh">
            <div id="formContent">
                <div class="fadeIn first">
                    <h3 class="py-4">{{ __('Repor Password') }}</h3>

                   
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>Escreva o seu email e enviamos um email de recuperação</p>

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="input-group">
                            <span><i class="fas fa-envelope" aria-hidden="true"></i></span>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">
                            {{ __('Enviar Link de Reposição de Password') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
