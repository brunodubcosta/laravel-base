@extends('layouts.backoffice_master_nm')

@section('content')
    <section id="login">
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <div class="fadeIn first">
                    <h3 class="py-4">{{ __('Repor Password') }}</h3>
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="input-group">
                            <span><i class="fas fa-envelope" aria-hidden="true"></i></span>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="input-group">
                            <span><i class="fas fa-lock" aria-hidden="true"></i></span>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="input-group">
                            <span><i class="fas fa-lock" aria-hidden="true"></i></span>
                            <input id="password-confirm" type="password" class="" name="password_confirmation" required autocomplete="new-password">
                        </div>
                        
                        <button type="submit" class="btn btn-primary">
                            {{ __('Repor Password') }}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
