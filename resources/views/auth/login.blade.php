@extends('layouts.backoffice_master_nm')

@section('head-meta')
	<title>{{ str_replace('.', ' ', config('app.name')) }} - Login</title>
@endsection



@section('content')
<section id="login">
	<div class="wrapper fadeInDown" style="height: 100vh">
		<div class="my-3">
			@include('flash::message')
		</div>
		<div id="formContent">
			<div class="fadeIn first">
                <div style="margin: 20px;">
                	<img id="img_logo" src="{{asset('/images/logo.png')}}" style="max-height: 18vh;" />
                </div>
                <div>
                    <!--<h3 style=" color: rgba(255, 255, 255, 0.7);text-shadow: 1px 1px 3px #000000; letter-spacing: 3px; "> {{ str_replace('.', ' ', config('app.name')) }}</h3> -->
                </div>
            </div>
			<form method="POST" action="{{ route('login') }}" id="loginform" name="loginform">
				@csrf
				<div class="input-group">
					<span><i class="fa fa-user" aria-hidden="true"></i></span>
					<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
					@error('email')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>

				<div class="input-group">
					<span><i class="fa fa-lock" aria-hidden="true"></i></span>
					<input class="fadeIn third" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
					@error('password')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
					@enderror
				</div>
				<button type="submit" class="fadeIn fourth" id="login1">
				{{ __('Login') }}
				</button>
			</form>
			<div id="formFooter">
				@if (Route::has('password.request'))
					<a class="btn btn-link" id="passwordReset" href="{{ route('password.request') }}">
					{{ __('Esqueceu-se da password?') }}
					</a>
				@endif
			</div>
		</div>
	</div>
</section>
@endsection

@section('foot-scripts')
	
@endsection
