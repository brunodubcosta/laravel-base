@extends('layouts.backoffice_master')

@section('head-meta')
	<title>{{ str_replace('.', ' ', config('app.name')) }} - {{ __('Alterar Password') }}</title>
@endsection

@section('head-script')
	{{-- expr --}}
@endsection

@section('content')
	<div class="row">
		<div class="col">
			<div id="change-password">
				{!! form($form) !!}
			</div>
		</div>
	</div>
@endsection

@section('foot-script')
	{{-- expr --}}
@endsection