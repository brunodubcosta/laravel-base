**Laravel Base Template**
- This is a simple template for using in new php projects. It includes a Backoffice and Frontoffice Template
---

## First steps

### 1. Clone this **repository** 
* You’ll start by cloning this **repository**

> `$ git clone https://bitbucket.org/brunodubcosta/laravel-base/src/master/`

### 2. Run Composer install 
* Then install/update all the requirements to run this project.

> `$ composer install`

### 3. Create a .env file
This is a file with all the enviroment constants.

> `$ cp .env.example .env`

Now open the .env file and setup a Database name for the project. 

For example:


```
	DB_DATABASE=base_db
	DB_USERNAME=root
	DB_PASSWORD=
```
	
### 4. Create a Database  
* Create a database named after the DB_DATABASE value saved in the .env file.

### 5. Generate a Key 
* The next thing you should do is set your application key to a random string by using this line:

> `$ php artisan key:generate`

---
## Recomendations

There are some recomendations to a better teamwork flow and better coding experiance.

1. For emailing: Builtin laravel Notifications, Emails or [PhpMailer](https://github.com/PHPMailer/PHPMailer "PhpMailer Github Page")
2. JS: JQuery
3. DB: MySql
4. Developing on windows: [Laragon](https://laragon.org/ "Laragon Page")

Good luck and Happy codings
---
